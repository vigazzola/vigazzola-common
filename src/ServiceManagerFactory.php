<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Common ;

use Psr\Container\ContainerInterface;
use Zend\ServiceManager\Config;
use Zend\ServiceManager\ServiceManager;

class ServiceManagerFactory {
    private static $container ;
    
    public static function setInstance(ContainerInterface $container) {        
        self::$container = $container ;
    }
    
    public static function getInstance($config = null) {        
        if(!self::$container) {
            $confProvider       = new ConfigProvider();
            $conf               = $confProvider() ;
            self::$container    = new ServiceManager();
            
            (new Config($conf['dependencies']))->configureServiceManager(self::$container);
            return self::$container ;
        }
        
        return self::$container ;
    }
  
}