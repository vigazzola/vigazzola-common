<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Common\Middleware;

use Psr\Container\ContainerInterface;

class SetupServiceManagerFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container, $response) 
    {
        return new SetupServiceManager($container) ;
    }
}
