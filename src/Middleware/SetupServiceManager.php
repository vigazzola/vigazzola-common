<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Common\Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;


class SetupServiceManager implements MiddlewareInterface
{
    private $container ;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container ;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
       \Vigazzola\Common\ServiceManagerFactory::setInstance($this->container) ;

        return $handler->handle($request);
    }
}
