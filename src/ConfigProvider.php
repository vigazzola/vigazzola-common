<?php
/**
 */

namespace Vigazzola\Common ;

class ConfigProvider
{
    /**
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencyConfig(),
        ];
    }

    /**
     *
     * @return array
     */
    public function getDependencyConfig()
    {
        return [
            'factories' => [
                Middleware\SetupServiceManager::class => Middleware\SetupServiceManagerFactory::class
            ],
        ];
    }
}
