# README #


Fornisce un metodo statico per avere in ogni punto del codice il ServiceManager pre i servizi IoC.
Chiamata:
$sm = \Vigazzola\Common\ServiceManagerFactory::getInstance() ;

Se nel file pipeline.php viene inserito il comando:
$app->pipe(\Vigazzola\Common\Middleware\SetupServiceManager::class);

getInstance() tornerà il ServiceManager Generale instanziato in container.php, altrimenti tornerà solo le configurazioni in \Vigazzola\*

Il metodo:
\Vigazzola\Common\ServiceManagerFactory::setInstance(\Psr\Container\ContainerInterface\ContainerInterface $container)

Permette di modificare il ServiceManager.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
